import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Random;

public class Test {
    private String name;
    private int amountOf;
    private HashMap<Integer, String> questions;
    private HashMap<Integer, ArrayList<String>> answers;
    private HashMap<Integer, Integer> correctAnswer;
    private HashMap<Integer, Integer> prices;

     public Test(String name, int amountOf, HashMap questions, HashMap answers, HashMap correctAnswer, HashMap prices){
         this.name = name;
         this.amountOf = amountOf;
         this.questions = questions;
         this.answers = answers;
         this.correctAnswer = correctAnswer;
         this.prices = prices;
     }

    public ArrayList<Integer> getOrder(int am){
        ArrayList<Integer> nums = new ArrayList<>();
        Random random = new Random();
        int amount = amountOf;
        for(int j = 0; j < amount; j++) {
            nums.add(j+1);
        }
        if(am == amountOf){
            return nums;
        }
        else {
            ArrayList<Integer> randomNums = new ArrayList<>();
            for (int i = am; i > 0; i--) {
                int k = random.nextInt(amount);
                randomNums.add(nums.get(k));
                nums.remove(k);
                nums.trimToSize();
                amount--;
            }
            return randomNums;
        }
    }

     public ArrayList<String> getTested(int amount){
         Scanner scanner = new Scanner(System.in);
         Date startDate = new Date();
         long start = startDate.getTime();
         ArrayList <String> info = new ArrayList<>();
         int score = 0;
         int maxScore = 0;
         int scoreCorrect = 0;

         ArrayList<Integer> nums = getOrder(amount);
         for(int i = 1; i <= amount; i++){
             int id = nums.get(i-1);
             System.out.println("Вопрос #" + i + ":");
             System.out.println(questions.get(id));
             System.out.println("Варианты ответов:");
             showAnswers(answers.get(id));
             int answer = Integer.parseInt(scanner.nextLine());
             maxScore = maxScore + prices.get(id);
             if(correctAnswer.get(id) == answer){
                 score = score + prices.get(id);
                 scoreCorrect++;
             }
             else{
                 info.add("Ошибка в вопросе #" +  i + "\n"
                         + questions.get(id) + "\n"
                         + "Ваш ответ: " + answer + "\n"
                         + "Правильный ответ: " + correctAnswer.get(id)+ "\n");
             }
         }

         Date endDate = new Date();
         long end = endDate.getTime();
         Date timer = new Date(end - start);
         info.add(0, "Тест пройден за: " + timer.getMinutes() + " минут " + timer.getSeconds() + " секунд" + "\n");
         info.add(1, "Поздравляем, вы набрали " + score + " балла(-ов) из " + maxScore + " возможных" + "\n");
         info.add(2, "Вы ответили верно на " + scoreCorrect + " вопроса(-ов) из " + amount + " возможных" + "\n");
         return info;
     }


    void showAnswers(ArrayList<String> answers){
        for(int i = 0; i < answers.size(); i++)  {
            System.out.println((i+1) + ") "+ answers.get(i));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmountOf() {
        return amountOf;
    }

    public void setAmountOf(int amountOf) {
        this.amountOf = amountOf;
    }

    public HashMap<Integer, String> getQuestions() {
        return questions;
    }

    public void setQuestions(HashMap<Integer, String> questions) {
        this.questions = questions;
    }

    public HashMap<Integer, ArrayList<String>> getAnswers() {
        return answers;
    }

    public void setAnswers(HashMap<Integer, ArrayList<String>> answers) {
        this.answers = answers;
    }

    public HashMap<Integer, Integer> getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(HashMap<Integer, Integer> correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public HashMap<Integer, Integer> getPrices() {
        return prices;
    }

    public void setPrices(HashMap<Integer, Integer> price) {
        this.prices = price;
    }
}
