import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class TestCenter {
    static final String MENU = "1. Добавить тест" + "\n" + "2. Пройти тест" + "\n" + "3. Выключить";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_CYAN = "\u001B[36m";

    public static void main(String[] args) {
        TestCenter testCenter = new TestCenter();
        Scanner scanner = new Scanner(System.in);
        ArrayList<Test> testList = new ArrayList<>();
        boolean on = true;
        while (on) {
            System.out.println(MENU);
            int answer = Integer.parseInt(scanner.nextLine());
            switch (answer) {
                case (1): {
                    try {
                        testList.add(testCenter.addTest());
                    }
                    catch(NumberFormatException e){
                        System.out.println(ANSI_RED + "Вводите данные корректно! Попробуйте ещё раз сначала" + ANSI_RESET);
                        testList.add(testCenter.addTest());
                    }
                    break;
                }
                case (2): {
                    System.out.println("Выберите номер теста (в скобках указано количество вопросов в тесте)");
                    testCenter.showTests(testList);
                    int ans1 = Integer.parseInt(scanner.nextLine())-1;
                    while ((ans1 >= testList.size()) || (ans1 < 0)){
                        System.out.println(ANSI_RED + "Теста под таким номером не существует. Попробуйте ещё раз!" + ANSI_RESET);
                        ans1 = Integer.parseInt(scanner.nextLine())-1;
                    }
                    System.out.println("Введите количество вопросов");
                    int ans2 = Integer.parseInt(scanner.nextLine());
                    while ((ans2 > testList.get(ans1).getAmountOf()) || (ans2 <= 0)){
                        System.out.println(ANSI_RED + "Введите корректное число вопросов (отличное от нуля и <= " + testList.get(ans1).getAmountOf() + " )" + ANSI_RESET);
                        ans2 = Integer.parseInt(scanner.nextLine());
                    }
                    testCenter.showResults(testList.get(ans1).getTested(ans2));
                    break;
                }
                case (3): {
                    on = false;
                    break;
                }
            }
        }
    }

    public Test addTest() throws NumberFormatException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Ввведите название теста");
        String name = scanner.nextLine();

        System.out.println("Ввведите количество вопросов");
        int n = Integer.parseInt(scanner.nextLine());

        HashMap<Integer, String> questions = new HashMap<>();
        HashMap<Integer, ArrayList<String>> answers = new HashMap<>();
        HashMap<Integer, Integer> correctAnswer = new HashMap<>();
        HashMap<Integer, Integer> prices = new HashMap<>();

        for (int i = 1; i <= n; i++) {
            System.out.println("Ввведите вопрос " + "#" + i);
            questions.put(i, scanner.nextLine());
            System.out.println("Ввведите количество ответов");
            int k = Integer.parseInt(scanner.nextLine());
            ArrayList<String> answersList = new ArrayList<>();
            for (int j = 1; j <= k; j++) {
                System.out.println("Ввведите ответ " + "#" + j);
                answersList.add(scanner.nextLine());
            }
            answers.put(i, answersList);
            System.out.println("Ввведите номер правильного ответа");
            int l = Integer.parseInt(scanner.nextLine());
            correctAnswer.put(i, l);
            System.out.println("Ввведите ценность вопроса");
            int m = Integer.parseInt(scanner.nextLine());
            prices.put(i, m);
            System.out.println("----------------------------------");
        }
        Test test = new Test(name, n, questions, answers, correctAnswer, prices);
        return test;
    }

    void showTests(ArrayList<Test> testList){
        for(int i = 0; i < testList.size(); i++){
            System.out.println((i+1) + ". " + testList.get(i).getName() + " (" + testList.get(i).getAmountOf() + ")");
        }
    }

    void showResults(ArrayList<String> results){
        System.out.println("Результаты теста:");
        for(int i = 0; i < results.size(); i++){
            if(i < 3) {
                System.out.println(ANSI_GREEN + results.get(i) + ANSI_RESET);
            }
            else{
                System.out.println(ANSI_CYAN + results.get(i) + ANSI_RESET);
            }
        }
    }
}
